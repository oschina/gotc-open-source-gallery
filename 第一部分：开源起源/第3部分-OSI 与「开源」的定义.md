## OSI 与「开源」的定义
### 1997 年 —— ESR 开始撰写《大教堂与集市》

1997 年，Linux 的一名开发者 ESR (Eric Steven Raymond) 陆续在网上发表了一些对 Linux 协作模式的洞见，后集成出版为《大教堂与集市》，该书被誉为“开源界圣经”、“开源运动的独立宣言”。

> 本书介绍了 Linux 的开源与协作模式，ESR 断言是这样的新式开发方式，使得 Linux 得以在专有软件盛行的背景下快速发展。它实用主义的角度给了一些人以启发，他们希望剔除自由软件概念中激进的东西，从实用的角度重新阐述自由软件的理念，让软件拥有更好的商业前景。
>
>![输入图片说明](https://oscimg.oschina.net/oscnet/up-69b8b91e70279bfed42149fd8aaba129dba.JPEG "在这里输入图片标题")

### 1998 年 —— 开放源代码促进会 (OSI) 成立

下面这张照片是 1998 年举办的"Open Source Summit"的会后合影。这场会议聚集的顶尖开源倡导者，以及其他开源先驱参与推动了 OSI 的创立，规范了「开源 (Open Source)」一词，并为开源软件的标准提出了明确的定义 (Open Source Definition, OSD)。

> ![输入图片说明](https://oscimg.oschina.net/oscnet/up-5ae7c54dbebdee3d3f8b3ab60aa1d436488.JPEG "在这里输入图片标题") 

**OSD**

| 条款 |	简单说明 |
| --- |	--- |
| Free Redistribution |	允许自由地再发布软件 |
| Source Code |	程序必须包含所有源代码 |
| Derived Works |	可以修改和派生新的软件 |
| Integrity of The Author's Source Code |	发布时保持软件源代码的完整性 |
| No Discrimination Against Persons or Groups |	不得歧视任何个人或团体 |
| No Discrimination Against Fields of Endeavor |	不得歧视任何应用领域（例如商业） |
| Distribution of License |	许可证的发布具有延续性 |
| License Must Not Be Specific to a Product |	许可证不能针对于某一个产品 |
| License Must Not Restrict Other Software |	许可证不能限制其他软件 |
| License Must Be Technology-Neutral |	许可证必须是技术中立的 |

> OSI 即开放源代码促进会（Open Source Initiative，缩写为 OSI），又译作开放源代码组织，是定义「开源」的非盈利组织，旨在推动开源软件发展。
> 
> ![输入图片说明](https://oscimg.oschina.net/oscnet/up-e4fad55581761af741bce1a5ef35137284c.png "屏幕截图.png")