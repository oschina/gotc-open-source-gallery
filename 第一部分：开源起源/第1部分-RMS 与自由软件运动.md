## RMS 与自由软件运动
### 1983 年 —— GNU 计划创立

理查德·马修·斯托曼（Richard Matthew Stallman，简称 RMS）认为：软件本该共享。他担心如果专有软件成为社会主流，会出现少数强力人物统治计算机工业的局面。为了捍卫软件的自由，RMS 决定重新开发一个完全由自由软件构建的操作系统，并将其命名为「GNU 计划」。

![输入图片说明](https://oscimg.oschina.net/oscnet/up-ddccf4eb6af9e5f1ea79edef64ea1b64119.JPEG "屏幕截图.png")

1983 年，RMS 在 net.unix-wizards 和 net.usoft 新闻组中公布「GNU 计划」，并借此发起自由软件运动。

> 自由软件
> 
> 自由软件的定义最初由 RMS 本人制定。自由软件的初衷是尊重用户的自由，并且尊重整个社区。粗略来讲，一个软件如果是自由软件，这意味着用户可以自由地运行、拷贝、分发、学习、修改并改进该软件。因此，「自由软件 (Free Software)」中的"free"是关乎自由的问题，与价格无关，软件如何定价并不影响它是否被归类为自由软件。具体来说，自由软件的用户拥有四项基本自由：
> 
> (0)自由运行软件
>
> (1)自由学习和修改软件源代码
>
> (2)自由发布软件拷贝
>
> (3)自由发布修改后的软件版本


> GNU
> GNU 是"GNU's Not Unix"的首字母递归缩写，意思是虽然 GNU 的设计与 UNIX 类似，但它不包含具有著作权的 UNIX 代码。它的发音为"Guh-NOO"，普通话可音译为“哥怒”。
>![输入图片说明](https://oscimg.oschina.net/oscnet/up-f2bb06f81c5c796dba3ce2d55d0e6392257.png "在这里输入图片标题")

### 1984 年 —— RMS 提出 Copyleft

Copyleft 是源自自由软件运动的概念，是一种利用现有著作权体制来保护所有用户和二次开发者的自由的授权方式。

Copyleft 中的"Left"，不使用英语中“保留”的意思，而是指“Left（左）”，与“版权(Copyright)”中的“Right（右）”具有镜像的关系。

二者的区别可总结为："Copyright"指软件的版权和其它一切权利归软件作者所私有，用户只有使用权，没有其它如复制、重新修改发布等权利。而"Copyleft"的特点是仅有版权归原作者所有，其他一切权利可以与任何人共享。

![Copyleft](https://oscimg.oschina.net/oscnet/up-5d41e7a854ad38374f2b19a4d95bfa825c3.png "屏幕截图.png")

### 1985 年 —— 自由软件基金会成立

在那个年代，任何排得上号的操作系统都包括命令解释器、汇编器、编译器、调试器、文本编辑器和电子邮件软件包等等。而开发一个操作系统是一个巨大的工程，耗时良久。为了能够实现这些目标，RMS 成立了自由软件基金会（Free Software Foundation，简写为 FSF），开始为「GNU 计划」募集基金。同时为「GNU 计划」提供技术、法律等支持。FSF 是历史上首个围绕自由与开源软件展开工作的公益性组织。
![自由软件基金会](https://oscimg.oschina.net/oscnet/up-553b592ecc0254bdeb91793006a4e3a8d16.png "屏幕截图.png")

### 1989 年 —— RMS 组织律师起草及发布 GPL

GNU 通用公共许可证 (GNU General Public License)，简称 GPL，是历史上首个开源软件许可协议。GPL 的出现为开源软件的版权带来了明确的定义，从法律层面实现了"Copyleft"概念的落地，也在不同程度上影响了后续出现的一系列开源软件许可协议。目前被广泛使用的 GPL 版本是 GPLv3。

![输入图片说明](https://oscimg.oschina.net/oscnet/up-91b899c362078f284fa927cefefc5e070ca.png "在这里输入图片标题")

> RMS 在美国麻州剑桥的麻省理工学院起草了第一份 GNU GPLv3 草案，在他右手边的是哥伦比亚法律教授伊本·莫格林——软件自由法律中心创始人及主席。
> 
> ![输入图片说明](https://oscimg.oschina.net/oscnet/up-3b9116e55963b33de772b5d38d0b43d9fb7.JPEG "在这里输入图片标题")

除了 GPL，目前主流的开源许可证包括：AGPL、Apache License、MIT License、BSD License、LGPL、MPL、EPL、Zlib License 等。

> 开源许可证介绍：https://opensource.org/licenses

### AGPL

GNU Affero 通用公共许可证 (GNU Affero General Public License, AGPL) 是 GPL 的一个补充，在 GPL 的基础上加了一些限制。

原有的 GPL/LGPL 协议由于提供 Web 服务公司的兴起（如：Google）产生了一定的漏洞（称之为 Web Service Loopwhole），比如某公司的软件使用了遵循 GPL 的开源项目，但是并不将该软件发布于 Web，则可以随心所欲地使用遵循 GPL 的开源代码，而无需开源自己的修改成果。AGPL 在 GPL 的基础上增加了对此做法的约束。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0618/102753_dcf48ca5_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-508ce1be9959fc06bfc7a14eb0312a0bb14.png

和 AGPL 类似的许可证还有 BSL 和 SSPL，前者通过了 OSI 认证，后者却没被 OSI 认可。

### BSL

BSL 是 MariaDB 公司创建的 License，它本质上是闭源和 Open Core 开源模式的“中间模式”，但也得到了 OSI 创始人 Bruce Perens 的认可。在 BSL 之下，源码始终是自由的，并且保证在某个时间点会变成“真的”开源（OSI 定义的开源）。

BSL 中指定级别以下的使用总是完全自由的，超过指定级别的使用需要有商业授权，直到滚动时间限制到期，这时所有对项目的使用行为都是自由的。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0618/115927_33050bef_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-fd73c544026f9ae59a4a5df7b070c04f77e.JPEG

### SSPL

SSPL 是 MongoDB 创建的 source-available 许可协议，它基于 GPLv3，并被认为是 Copyleft License，旨在体现开源的同时为产品应对云厂商提供保护，防止云厂商将开源产品作为服务提供而不为之回馈。SSPL 允许自由和不受限制地使用和修改产品源代码，简单的要求是，如果你将产品作为服务提供给他人，那么必须在 SSPL 下公开发布任何修改以及管理层的源代码。

不过有观点认为 SSPL 是为歧视特定用户而设计，在商业用户间会引起恐惧、不确定性及失望（Fear、Uncertainty、Doubt，简称 FUD）。因此它并没有通过 OSI 的认证。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0622/183341_f515e7dc_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-9581360b96ae1e63e4368301996b79f665a.png

### Apache License

Apache License 是一个由 Apache 软件基金会发布的自由软件许可证，最初为 Apache http 服务器而撰写。Apache License 要求被授权者保留著作权和放弃权利的声明，但它不是一个反著作权的许可证。Apache License 在 Apache 社区内外被广泛使用，Apache 软件基金会下属所有项目均使用 Apache License。

Apache License 最新的版本是 2.0，发布于2004年。最原始的版本是 1.0，后续在此基础上发布了 1.1。

目前被广泛使用的是 Apache License 2.0，其与 GPLv3 兼容。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/102505_f3812319_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-1955d365f12345c9b52f9829d1f108be91f.png

### MIT License

MIT 是诸多开源许可证中被广泛使用的其中一种，与其他常见的许可证（如 GPL、LGPL、BSD）相比，MIT 是相对宽松的开源许可证，并且与 GPL 兼容。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/102955_69a6d83a_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-d45af779462ad14f173e00f996eca0b29a7.png

### BSD License

BSD License 也是被广泛使用的开源许可证之一。由于 BSD 操作系统遵循此许可证进行发布，故而得名 BSD License，目前分为原始版 BSD License 和修改版 BSD License。

BSD License 比较宽松，甚至跟公有领域更为接近。事实上，BSD License 被认为是 Copycenter（中间著作权），介乎标准的 Copyright 与 GPL 的 Copyleft 之间。BSD License 鼓励代码共享，但需要尊重代码作者的著作权。由于 BSD License 允许使用者修改和重新发布代码，也允许使用或在 BSD 代码上开发商业软件发布和销售，因此 BSD License 是对商业集成很友好的协议。很多的公司企业在选用开源产品的时候都首选 BSD License，因为可以完全控制第三方的代码，在必要的时候可以修改或者二次开发。

目前被广泛使用的是 BSD 3-Clause "New" or "Revised" License 和 BSD 2-Clause "Simplified" or "FreeBSD" License。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/111919_b63bb47a_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-a7cb0571d7b7da34c3455b26b23dc5e4541.png

### LGPL

LGPL 是自由软件基金会公布的自由软件许可证，全称为 GNU Lesser General Public License，即为更宽松的 GPL。

LGPL 属于 GPL 的变种，也是 GNU 为了得到更多的甚至是商用软件开发商的支持而提出的。与 GPL 的最大不同是，在使用了 LGPL 的项目中可以私有使用 LGPL 授权的自由软件，开发出来的新软件可以是私有的而不需要是自由软件。所以任何公司在使用自由软件之前应该保证在 LGPL 或其它 GPL 变种的授权下。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/115109_911125f1_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-a7d6909b843fce3e0c335826ca361bdf498.png

### MPL

Mozilla 公共许可证（英语：Mozilla Public License，简称 MPL）是自由开源的软件许可证，由 Mozilla 基金会开发并维护。该协议融合了 BSD License 和 GPL 的特性，追求平衡专有软件和开源软件开发者之间的顾虑。

MPL 既是自由软件基金会承认的自由软件许可证，也是经开放源代码促进会认证的开源软件许可证。MPL 允许在其授权下的源代码与其他授权的文件进行混合，包括私有许可证。但在 MPL 授权下的代码文件必须保持 MPL 授权，并且保持开源。

目前被广泛使用的是 Mozilla Public License 2.0。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/115557_0cbe0188_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-61e42d98d0a91402810f33f7a93622f347b.png

### EPL

Eclipse 公共许可证（英语：Eclipse Public License，简称 EPL）是商业友好的开源许可证，最初被 Eclipse 基金会应用于名下的集成开发环境 Eclipse 上。EPL替代了原先的通用公共许可证（Common Public License，简称 CPL），在其基础上删除了专利相关诉讼的限制条款。在使用以 EPL 授权的程序时，用户有权使用、修改、复制与传播软件原始版本和修改后版本，在某些情况下则必须将修改内容一并发布。

EPL 已通过开放源代码促进会的认证，同时亦由自由软件基金会列入自由软件许可证名单。

目前被广泛使用的是 Eclipse Public License version 2.0。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/121630_48bfb7f3_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-4e400a15fb3dc78153b7b9b6172353ae882.png

### Zlib License

Zlib License 是一个自由软件许可证，它兼容 GPL，并已通过开放源代码促进会的认证。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/122005_4ce39f87_755420.png "屏幕截图.png")

> https://oscimg.oschina.net/oscnet/up-01b4d58d3a2690fc93c354a35113bb79161.png

上面提到的开源许可证均诞生于国外。2019 年，中国首个开源许可证正式发布，名为木兰宽松许可证 (Mulan Permissive Software License, Mulan PSL)。2020 年，OSI 批准了来自中国的 MulanPSL 2.0，木兰宽松许可证（第 2 版）正式成为一个国际化开源许可证。

### 木兰宽松许可证

木兰开源许可证作为国家重点研发计划“云计算和大数据开源社区生态系统”的子任务，由北京大学牵头，依托全国信标委云计算标准工作组和中国开源云联盟，联合国内开源生态圈产学研各界优势团队、开源社区以及拥有丰富知识产权相关经验的众多律师，共同研制而成。

木兰开源许可证最初版本《木兰宽松许可证，第 1 版》于 2019 年 8 月 5 日发布，发布后为国内众多开发者陆续采用。Mulan PSL 采用中英文表达，中英文表述具有同等法律效力，简化了中国使用者进行法律解释时的复杂度；许可证针对现有诉讼问题更清晰地明确版权、专利和商标授权，提供更完善的法律保护；许可证遵从表述简洁原则，去除冗余，条款简洁，容易理解，兼容性好。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0618/123234_f059c241_755420.png "屏幕截图.png")

> https://images.gitee.com/uploads/images/2021/0618/123234_f059c241_755420.png