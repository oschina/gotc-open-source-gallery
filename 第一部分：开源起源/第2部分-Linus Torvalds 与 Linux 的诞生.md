> 到了 1990 年，「GNU 计划」已经开发出的软件包括了一个功能强大的文字编辑器 Emacs、C 语言编译器 GCC 以及大部分 UNIX 系统的程序库和工具。唯一没有完成的重要组件，就是操作系统的内核。

## Linus Torvalds 与 Linux 的诞生
### 1991 年 —— Linux 内核诞生

1991 年，21 岁的芬兰学生 Linus Benedict Torvalds 在 comp.os.minix 新闻组宣布 Linux 项目，并将源代码分享到了互联网上。

![](https://oscimg.oschina.net/oscnet/up-29185d58519a5f372ffa7bef2b0af1d5d86.JPEG)

由于 Linux 具有结构清晰、功能简捷等特点，吸引了许多高校的学生和科研机构的研究人员把它作为学习和研究的对象。他们在更正原有 Linux 版本中问题的同时，也不断地为 Linux 增加新的功能。在众多热心者的努力下，Linux 逐渐成为一个稳定可靠、功能完善的操作系统内核，并顺理成章地成为了 GNU 计划中的操作系统内核。

![](https://oscimg.oschina.net/oscnet/up-036b57c2c51aa63bec41f79f8307c1f3d9c.JPEG)

以 Linux 内核+ GNU 自由软件组成的 GNU/Linux 操作系统成为了具有共享精神的开发者们进行自由软件开发的平台，越来越多的自由软件在 GNU/Linux 上被开发出来。

与此同时，来自全球各地的开发者加入到 Linux 社区中贡献代码，共同优化 Linux 内核，日常开发相关的讨论也在 Linux 内核邮件列表上进行。

### 1994 年 —— Linux 内核 1.0 发布

1994 年 3 月 14 日，Linux 内核 1.0 正式发布，共包含 176,250 行代码。随后的 1995 年 3 月，包含 310,950 行代码的 Linux 内核 1.2.0 发布。


![](https://oscimg.oschina.net/oscnet/up-ad8e5c4c6d8f8ba9a3728a93b4230295713.png)