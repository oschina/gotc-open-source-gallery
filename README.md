> GOTC 2021 已经圆满落幕，本仓库作为“开源长廊”的内容载体，本身也是意义非凡，仓库将持续更新，欢迎参与。

> 线上版本正在规划中。

# GOTC-开源长廊是什么？

通过展示开源生态中基金会、开源项目、开源人物、技术社区、厂商开源贡献，为广大开发者展开一幅全球开源生态画卷，以及开源几十年发展中诞生的璀璨群星。

完成后的「开源长廊」以文化墙的形式展现在 GOTC 全球开源技术峰会现场，会议现场设置左右两条长廊（长 >=50*2 米），墙壁上专门用于展示「开源长廊」的内容。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0712/175358_794eb30e_7885804.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0712/175432_a46b9776_7885804.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0712/175504_6e1efcb0_7885804.png "屏幕截图.png")

（GOTC 2021 上海站场馆中搭建的开源长廊）


 **「开源长廊」项目中的各部分内容将以开源的形式协作完成，欢迎广大开源爱好者或相关从业者积极参与贡献，在这幅史无前例的开源画卷上留下浓墨重彩的一笔。** 


# 建设开源长廊

## 长廊组成图示


**整个长廊都由类似以下的【图文卡片】与【旁白文字】组成。** 


![输入图片说明](https://images.gitee.com/uploads/images/2021/0604/172455_19af5052_755420.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0604/172343_c1aa2eee_755420.png "屏幕截图.png")


## 公开征集模块

**此仓库用于公开征集长廊相关信息，你可以提交以下模块信息：**
- 如果你知道哪些公司拥抱开源 【提交信息】 ==> 
    - 国际：[国外积极参与开源的公司](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%BA%8C%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E9%99%85%E5%BC%80%E6%BA%90%E8%B6%8B%E5%8A%BF/%E7%AC%AC2%E9%83%A8%E5%88%86-%E7%A7%AF%E6%9E%81%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%E7%9A%84%E5%85%AC%E5%8F%B8.md)
    - 国内：[国内积极参与开源的公司](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%B8%89%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E5%86%85%E5%BC%80%E6%BA%90%E5%8F%91%E5%B1%95/%E7%AC%AC2%E9%83%A8%E5%88%86-%E7%A7%AF%E6%9E%81%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90%E7%9A%84%E5%85%AC%E5%8F%B8.md)
- 如果你知道开源与技术社区有哪些  【提交信息】 ==> 
    - 国际：[国际社区](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%BA%8C%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E9%99%85%E5%BC%80%E6%BA%90%E8%B6%8B%E5%8A%BF/%E7%AC%AC3%E9%83%A8%E5%88%86-%E5%9B%BD%E9%99%85%E7%A4%BE%E5%8C%BA.md)
    - 国内：[国内社区](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%B8%89%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E5%86%85%E5%BC%80%E6%BA%90%E5%8F%91%E5%B1%95/%E7%AC%AC5%E9%83%A8%E5%88%86-%E7%A4%BE%E5%8C%BA.md)
- 如果你知道知名开源开源基金会有哪些  【提交信息】 ==> [开源基金会](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%BA%8C%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E9%99%85%E5%BC%80%E6%BA%90%E8%B6%8B%E5%8A%BF/%E7%AC%AC1%E9%83%A8%E5%88%86-%E5%9B%BD%E9%99%85%E5%BC%80%E6%BA%90%E5%9F%BA%E9%87%91%E4%BC%9A.md)
- 如果你知道国内的一些开源大事件  【提交信息】 ==> [开源大事件](https://gitee.com/h4cd/gotc-open-source-gallery/blob/master/%E7%AC%AC%E4%B8%89%E9%83%A8%E5%88%86%EF%BC%9A%E5%9B%BD%E5%86%85%E5%BC%80%E6%BA%90%E5%8F%91%E5%B1%95/%E7%AC%AC0%E9%83%A8%E5%88%86-%E5%9B%BD%E5%86%85%E5%BC%80%E6%BA%90%E5%A4%A7%E4%BA%8B%E4%BB%B6.md)

当然，你还可以提交「开源长廊」中的其它模块内容，总有一处你可以发挥。

欢迎提交你所知道的相关内容，通过审核并最终调整后，你提交的内容将出现在「开源长廊」中，为全球观众所了解，同时你将成为此项目的贡献者。

## 贡献指南


### 贡献规则

- 提交内容中如果包含图片，请提供高清版本，并同时提供原始链接
- 内容格式使用 Markdown

### 贡献方式

- 直接通过轻量级 PR 提交
- 点击具体文档右侧“编辑”按钮提交即可：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0607/174639_c629f094_755420.png "屏幕截图.png")


---

**GOTC**，the Global Opensource Technology Conference，全球开源技术峰会。

[https://gotc.oschina.net](https://gotc.oschina.net)

[全球开源技术峰会 GOTC 2021 圆满落幕](https://www.oschina.net/news/154628/gotc-2021-review)